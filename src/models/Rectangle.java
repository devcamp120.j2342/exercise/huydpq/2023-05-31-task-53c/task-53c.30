package models;

public class Rectangle extends Shape {
    protected double width = 1.0;
    protected double height = 1.0;

    public Rectangle() {
        super();
    }
    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }
    public Rectangle(String color, Boolean filled, double width, double height) {
        super(color, filled);
        this.width = width;
        this.height = height;
    }
    public double getWidth() {
        return width;
    }
    public void setWidth(double width) {
        this.width = width;
    }
    public double getHeight() {
        return height;
    }
    public void setHeight(double height) {
        this.height = height;
    }
    
    // diện tích hình chữ nhật
    public double getArea(){
        return this.height * this.width;
    }

    //  chu vi  hình chữ nhật
    public double getPerimeter(){
        return (this.height + this.width) * 2;
    }
    @Override
    public String toString() {
        return "Rectangle " + super.toString()+ ", width=" + width + ", height=" + height  +"]";
    }

    
}
