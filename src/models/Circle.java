package models;

public class Circle extends Shape {
    protected double radius = 1.0;

    public Circle() {

    }
    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(String color, Boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }
    public double getRadius() {
        return radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }
    // diện tích hình tròn
    public double getArea(){
       return Math.PI * Math.pow(this.radius,2);
    }
    // chu vi hình tròn
    public double getPerimeter(){
       return Math.PI * this.radius *2 ;

    }
    @Override
    public String toString() {
        return "Circle " + super.toString()+" , radius=" + radius + "]";
    }

    
}
