import javax.swing.plaf.synth.SynthOptionPaneUI;

import models.Circle;
import models.Rectangle;
import models.Shape;
import models.Square;

public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle = new Rectangle(3,4); // hình chữ nhật
        Circle circle = new Circle(2); // hình tròn
        Square square = new Square(3); // hình vuông

        System.out.println(rectangle);
        System.out.println(circle);
        System.out.println(square);

        System.out.println("Hình chữ nhật");
        System.out.println(rectangle.getArea());
        System.out.println(rectangle.getPerimeter());

        System.out.println("Hình tròn");
        System.out.println(circle.getArea());
        System.out.println(circle.getPerimeter());

        System.out.println("Hình vuông");
        System.out.println(square.getArea());
        System.out.println(square.getPerimeter());



    }
}
